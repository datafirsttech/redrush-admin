import Vue from 'vue'   
import Vuex, { Store } from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Store({
    state:{
        user: '',         

    },
    mutations: {
        GET_USER_INFO(state){
            alert(state.user)
        },

        SET_USER_DATA(state, userData){
            console.log(userData);
            state.user = userData;
            localStorage.setItem('USER_DATA', JSON.stringify(userData))
            axios.defaults.headers.common['authurization'] = `Bearer ${userData.token}`
        },
    },
    actions: {
        login({commit}, credentials){
            commit('SET_USER_DATA', credentials)
        },

        register({},credentials){
            console.log(credentials);
            return axios.post('http://localhost:3001/register', credentials )
            .then(result =>{
                console.log(result);
            }).catch(err =>{
                console.log(err.response);
            })
        },
        
    },
    getters:{
      
    }
})