import axios from 'axios'

const apiClient = axios.create({
    baseURL: 'http://localhost:3001',
    withCcredentials: false,
    headers:{
        Accept: 'application/json',
        'Content-Type': 'application/json'
    }
})

export default{
    loginUser(credentials){
        return apiClient.post('/login', credentials)
    },
    // Register()
}