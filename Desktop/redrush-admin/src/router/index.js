import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/components/Dashboard'
import Login from '@/components/auth/login'
import Register from '@/components/auth/Register'
import ChangePassword from '@/components/auth/change-password'
import ResetPassword from '@/components/auth/reset-password'
import nProgress from 'nprogress'

Vue.use(Router)

const router =  new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/change-password',
      name: 'ChangePassword',
      component: ChangePassword
    },
    {
      path: '/reset-password',
      name: 'ResetPassword',
      component: ResetPassword
    },
  ]
})

router.beforeEach((routeTo, routeFrom, next)=>{
   nProgress.start()
   next()
})

router.afterEach(()=>{
  nProgress.done()
})

export default router